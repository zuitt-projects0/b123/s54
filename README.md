To start using the built-in tests: 
run <code>npm install</code> first then issue the <code>npm test</code> at the root directory of this project.

WARNING: Do not change any code inside <code>test.js</code>.

## Pushing Instructions

### If you're done, create a new repo in your zuitt folder: s54.

### Delete the .git folder for your local s54.

### Initialize a new git in the s54 folder:
	git init
### Add your updates: 
	git add .
### Commit your changes: 
	git commit -m "includes solution Data Structure Mock Technical Exam".
### Connect your remote s54 repo to this local repo: 
	git remote add origin <url>
### Push your updates:
	git push origin master

### Link your s54e repo to boodle:
	WD078-54 | Mock Technical Exam (Data Structures and Algorithms)
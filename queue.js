let collection = [];

//Write the queue functions below.
//NOTE: DO NOT USE ANY ARRAY METHODS. YOU CAN USE .length property

// List of array methods that you SHOULD NOT use:
// concat(), copyWithin(), entries(), every(), fill(), filter(), find(), findIndex(), from(), includes(), indexOf(), isArray(), join(), keys(), lastIndexOf(), map(), pop(), push(), reduce(), reduceRight(), reverse(), shift(), slice(), some(), sort(), splice(), toString(), unshift(), valueOf()

//Exception: forEach()
//NOTE: USE return KEYWORD AND NOT console.log() FOR RETURNING VALUES


function print(){

	// Return the value of the array
	return collection

}


function enqueue(element){

	// Add element at the end of the queue
	// Return the value of the array
	// With an array method: .push()
	// Note: Do not use array methods except forEach()
	for(let i = 0; i<=collection.length; i++){
		if(i === collection.length){
			collection[i] = element
			return collection
		}
	}


}


function dequeue(){

	// Remove the first element of the collection array 
	// Return the updated/manipulated collection array
	// With array method: shift()
	// Note: Do not use array methods except forEach()

	let [, ...rest] = collection;
	collection = rest
	return collection
	
}

function front(){

	//Return the first item in the collection array

	return collection[0]

}

function size(){

	//Return the current number of items in the array
	let item = collection.length
	return item

}

function isEmpty(){

	//Check if the array is empty or not and return a boolean.
	if(collection === null){
		return true
	}
	else{
		return false
	}
}



module.exports = {

print,
enqueue,
dequeue,
front,
size,
isEmpty

};